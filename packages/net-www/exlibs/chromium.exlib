# Copyright 2009, 2010 Elias Pipping <pipping@exherbo.org>
# Copyright 2009 Heiko Przybyl <zuxez@cs.tu-berlin.de>
# Copyright 2016-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# You'll find new releases here:
# - http://omahaproxy.appspot.com/viewer
# - http://googlechromereleases.blogspot.com

CHROMIUM_CHANNEL=${PN#chromium-}

MY_PN=chromium
MY_PNV=${MY_PN}-${PV}

require freedesktop-desktop gtk-icon-cache python [ has_lib=false ] toolchain-funcs flag-o-matic

export_exlib_phases pkg_setup src_prepare src_configure src_compile src_test src_install pkg_postinst pkg_postrm

SUMMARY="The open-source project behind Google Chrome"
DESCRIPTION="Browser based on Blink and Google's V8 JavaScript engine."
HOMEPAGE="https://www.chromium.org/Home"
DOWNLOADS="
    https://storage.googleapis.com/chromium-browser-official/${MY_PNV}.tar.xz
    https://gsdview.appspot.com/chromium-browser-official/${MY_PNV}.tar.xz
"

LICENCES="
    BSD-3         [[ note = [ chromium itself ] ]]
    Apache-2.0    [[ note = [ skia ] ]]
    BSD-2         [[ note = [ bsdiff, bspatch ] ]]
    BSD-3         [[ note = [ dtoa, jscre, modp_b64, v8, webkit ] ]]
    GPL-2         [[ note = [ hunspell, 'Mozilla interface to Java Plugin APIs', npapi, nspr, nss ] ]]
    LGPL-2        [[ note = [ webkit ] ]]
    LGPL-2.1      [[ note = [ hunspell, 'Mozilla interface to Java Plugin APIs', npapi, nspr,
                              nss, webkit ] ]]
    MPL-1.1       [[ note = [ hunspell, 'Mozilla interface to Java Plugin APIs', npapi, nspr, nss ] ]]
    MIT           [[ note = [ harfbuzz ] ]]
    public-domain [[ note = [ lzma_sdk, sqlite ] ]]
"
SLOT="0"
MYOPTIONS="
    bindist
    clang-build [[ description = [ Force building with clang (recommended) ] ]]
    cups
    keyring [[ description = [ Store passwords using GNOME Keyring ] ]]
    pulseaudio
    suid-sandbox [[ description = [ Enable Chromium's SUID sandbox, only necessary for
        kernels built without user namespaces ] ]]
    platform:
        amd64
        x86
    x86_cpu_features:
        sse2
    ( providers: jpeg-turbo )
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# http://crbug.com/62803 tracks the addition of a use_openssl option. Once that bug is closed,
# use_openssl=1 can be passed; dev-libs/{nss,nspr} will no longer be needed. Note that that flag
# currently does not use system openssl but a bundled version from third_party/openssl!
# There is use_system_ssl for that

# TODO: GN build with system speech dispatcher and libevent
#  app-speech/speechd[>=0.8]
#  dev-libs/libevent[>=1.4.13]
DEPENDENCIES="
    build:
        dev-lang/node
        dev-lang/python:*[>=2.5&<3]
        dev-lang/yasm
        dev-perl/Switch
        dev-python/beautifulsoup4[python_abis:2.7]
        dev-python/html5lib[python_abis:2.7]
        dev-python/simplejson[python_abis:2.7]
        dev-scm/git [[ note = [ https://bugs.gentoo.org/show_bug.cgi?id=593476 ] ]]
        dev-util/gperf[>=3.0.4]
        sys-devel/bison
        sys-devel/ninja[>=1.7.2]
        clang-build? ( dev-lang/clang )
    build+run:
        app-arch/gzip[>=1.8]
        app-arch/snappy
        dev-libs/at-spi2-atk
        dev-libs/atk
        dev-libs/dbus-glib:1[>=0.84]
        dev-libs/expat
        dev-libs/glib:2[>=2.26] [[ note = [ for gsettings ] ]]
        dev-libs/icu:=[>=59]
        dev-libs/libxml2:2.0[>=2.9.6-r1] [[ note = [ -r1 for icu support ] ]]
        dev-libs/libxslt[>=1.1.26]
        dev-libs/nspr[>=4.0]
        dev-libs/nss[>=3.26] [[ note = [ see crypto/nss_util.cc for the version requirement ] ]]
        dev-libs/re2
        media/ffmpeg[>=3.0]
        media-libs/flac
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libpng:=[>=1.2.37]
        media-libs/libvpx:5
        media-libs/libwebp:=[>=0.6.0-r1]
        media-libs/openh264[>=1.6.0]
        media-libs/opus[>=1.1-r1]
        net-libs/libsrtp
        sys-apps/dbus
        sys-apps/pciutils
        sys-libs/zlib[>=1.2.7-r1]
        sys-sound/alsa-lib
        virtual/usb:1
        x11-dri/libdrm
        x11-libs/cairo[X]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/harfbuzz[>=1.5.0]
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXScrnSaver
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/pango
        cups? (
            dev-libs/libgcrypt
            net-print/cups
        )
        keyring? ( gnome-desktop/libgnome-keyring:1 )
        providers:eudev? ( sys-apps/eudev )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:systemd? ( sys-apps/systemd )
        pulseaudio? ( media-sound/pulseaudio )
    run:
        x11-libs/gtk+:3
        cups? ( x11-libs/gtk+:3[cups] )
    recommendation:
        fonts/corefonts [[ description = [ Needed to display some websites ] ]]
        x11-apps/xdg-utils [[ description = [ Set as default browser. Use application launchers for downloaded files (only for Gnome/KDE/Xfce) ] ]]
    suggestion:
        gnome-desktop/adwaita-icon-theme [[ description = [ For the downloads tab ] ]]
"

WORK=${WORKBASE}/${MY_PNV}

eninja() {
    edo ninja -j ${EXJOBS:-1} -v "$@"
}

chromium_pkg_setup() {
    export V=1

    exdirectory --allow /opt

    if option clang-build; then
        export CC=$(exhost --target)-clang
        export CXX=$(exhost --target)-clang++
    fi

    if cxx-is-gcc && ! ever at_least 5 $(gcc-version) ; then
        eerror "sys-devel/gcc[>=5] is required to build chromium-stable."
        die
    fi

    if cxx-is-gcc; then
        ewarn "Please be aware that Chromium's compilation with GCC tends"
        ewarn "to break sometimes. While we try to backport relevant patches"
        ewarn "to have a working compilation with GCC ( if there are any )"
        ewarn "we don't want to keep back releases which potentially fix"
        ewarn "major CVEs for GCC compitibility. You can enable the clang-build"
        ewarn "option to force building chromium-stable with clang."
        ewarn "You can also do it with eclectic:"
        ewarn "# eclectic cc set clang"
        ewarn "# eclectic c++ set clang"
    fi
}

chromium_src_prepare() {
    default

    # setup nodejs
    edo mkdir -p third_party/node/linux/node-linux-x64/bin
    edo ln -s /usr/host/bin/node third_party/node/linux/node-linux-x64/bin/node

    local keeplibs=(
        base/third_party/dmg_fp
        base/third_party/dynamic_annotations
        base/third_party/icu
        base/third_party/nspr
        base/third_party/superfasthash
        base/third_party/symbolize
        base/third_party/valgrind
        base/third_party/xdg_mime
        base/third_party/xdg_user_dirs
        chrome/third_party/mozilla_security_manager
        courgette/third_party
        net/third_party/mozilla_security_manager
        net/third_party/nss
        third_party/WebKit
        third_party/analytics
        third_party/angle
        third_party/angle/src/common/third_party/base
        third_party/angle/src/common/third_party/smhasher
        third_party/angle/src/third_party/compiler
        third_party/angle/src/third_party/libXNVCtrl
        third_party/angle/src/third_party/trace_event
        third_party/blink
        third_party/boringssl
        third_party/boringssl/src/third_party/fiat
        third_party/breakpad
        third_party/breakpad/breakpad/src/third_party/curl
        third_party/brotli
        third_party/cacheinvalidation
        third_party/catapult
        third_party/catapult/third_party/polymer
        third_party/catapult/common/py_vulcanize/third_party/rcssmin
        third_party/catapult/common/py_vulcanize/third_party/rjsmin
        third_party/catapult/tracing/third_party/d3
        third_party/catapult/tracing/third_party/gl-matrix
        third_party/catapult/tracing/third_party/jszip
        third_party/catapult/tracing/third_party/mannwhitneyu
        third_party/catapult/tracing/third_party/oboe
        third_party/catapult/tracing/third_party/pako
        third_party/ced
        third_party/cld_3
        third_party/crc32c
        third_party/cros_system_api
        third_party/devscripts
        third_party/devtools-node-modules
        third_party/dom_distiller_js
        third_party/fips181
        third_party/flatbuffers
        third_party/flot
        third_party/freetype
        third_party/glslang-angle
        third_party/google_input_tools
        third_party/google_input_tools/third_party/closure_library
        third_party/google_input_tools/third_party/closure_library/third_party/closure
        third_party/googletest
        third_party/hunspell
        third_party/iccjpeg
        third_party/ink
        third_party/inspector_protocol
        third_party/jinja2
        third_party/jstemplate
        third_party/khronos
        third_party/leveldatabase
        third_party/libXNVCtrl
        third_party/libaddressinput
        third_party/libjingle
        third_party/libphonenumber
        third_party/libsecret
        third_party/libsrtp
        third_party/libudev
        third_party/libusb
        third_party/libwebm
        third_party/libxml/chromium
        third_party/libyuv
        third_party/libovr
        third_party/llvm
        third_party/lss
        third_party/lzma_sdk
        third_party/markupsafe
        third_party/mesa
        third_party/metrics_proto
        third_party/modp_b64
        third_party/mt19937ar
        third_party/node
        third_party/node/node_modules/polymer-bundler/lib/third_party/UglifyJS2
        third_party/openmax_dl
        third_party/ots
        third_party/pdfium
        third_party/pdfium/third_party/agg23
        third_party/pdfium/third_party/base
        third_party/pdfium/third_party/build
        third_party/pdfium/third_party/bigint
        third_party/pdfium/third_party/freetype
        third_party/pdfium/third_party/lcms
        third_party/pdfium/third_party/libopenjpeg20
        third_party/pdfium/third_party/libpng16
        third_party/pdfium/third_party/libtiff
        third_party/perfetto
        third_party/ply
        third_party/polymer
        third_party/protobuf
        third_party/protobuf/third_party/six
        third_party/qcms
        third_party/sfntly
        third_party/s2cellid
        third_party/skia
        third_party/skia/third_party/gif
        third_party/skia/third_party/vulkan
        third_party/smhasher
        third_party/spirv-headers
        third_party/spirv-tools-angle
        third_party/sqlite
        third_party/swiftshader
        third_party/swiftshader/third_party/llvm-subzero
        third_party/swiftshader/third_party/subzero
        third_party/tcmalloc
        third_party/usrsctp
        third_party/vulkan
        third_party/vulkan-validation-layers
        third_party/web-animations-js
        third_party/webdriver
        third_party/webrtc
        third_party/widevine
        third_party/woff2
        third_party/zlib/google
        url/third_party/mozilla
        v8/src/third_party/utf8-decoder
        v8/src/third_party/valgrind
        v8/third_party/inspector_protocol
    )

    # TODO: keep for GN builds
    keeplibs+=(
        base/third_party/libevent
        third_party/adobe
        third_party/speech-dispatcher
        third_party/usb_ids
        third_party/xdg-utils
        third_party/yasm/run_yasm.py
    )

    # TODO: Find out how to properly do this for GN and/or report upstream
    edo sed \
        -e "s:pkg_config = \"\":pkg_config = \"$(exhost --tool-prefix)pkg-config\":g" \
        -i build/config/linux/pkg_config.gni

    # TODO: Find out how to properly do this for GN and/or report upstream
    # Fix this for multiarch by adding appropriate symlinks here and adding this directory to
    # the PATH
    local dir=${WORKBASE}/symlinked-build-tools
    edo mkdir -p ${dir}
    # Build error: FAILED: x64/obj/third_party/brotli/libbrotli.a
    edo ln -s /usr/host/bin/$(exhost --target)-ar ${dir}/ar
    # Build error: FAILED: x64/obj/third_party/brotli/brotli/decode.o
    edo ln -s /usr/host/bin/"${CC}" ${dir}/gcc
    # Build error: FAILED: x64/obj/third_party/brotli/bro/encode_parallel.o
    edo ln -s /usr/host/bin/"${CXX}" ${dir}/g++
    # Build error: strip is hardcoded somewhere
    edo ln -s /usr/host/bin/$(exhost --target)-strip ${dir}/strip
    # Build error: FAILED: libGLESv2.so libGLESv2.so.TOC
    edo ln -s /usr/host/bin/$(exhost --target)-readelf ${dir}/readelf

    # and, they assume python is python2!
    edo ln -s /usr/host/bin/python2 ${dir}/python
    export PATH="${dir}:${PATH}"

    # remove some of the bundled libraries
    edo build/linux/unbundle/remove_bundled_libraries.py "${keeplibs[@]}" --do-remove
}

chromium_src_configure() {
    # libevent: https://bugs.gentoo.org/593458
    local gn_system_libraries="
        ffmpeg
        flac
        fontconfig
        icu
        libdrm
        libjpeg
        libpng
        libvpx
        libwebp
        libxml
        libxslt
        openh264
        opus
        re2
        snappy
        yasm
        zlib
    "

    edo build/linux/unbundle/replace_gn_files.py --system-libraries ${gn_system_libraries}

    local myconf_gn=""

    export AR="${AR}"
    export CC="${CC}"
    export CXX="${CXX}"
    export NM="${NM}"

    # Define a custom toolchain for GN
    myconf_gn+=" custom_toolchain=\"//build/toolchain/linux/unbundle:default\""

    # GN needs explicit config for Debug/Release as opposed to inferring it from build directory
    myconf_gn+=" is_debug=false"

    # Use tcmalloc
    myconf_gn+=" use_allocator=\"tcmalloc\""

    # Disable nacl, we can't build without pnacl (http://crbug.com/269560)
    myconf_gn+=" enable_nacl=false"

    # Trying to use gold results in linker crash
    myconf_gn+=" use_gold=false use_sysroot=false linux_use_bundled_binutils=false use_custom_libcxx=false"

    # We want to keep this disabled for now, even if we use clang, fails to build:
    # error: unable to load plugin '../../third_party/llvm-build/Release+Asserts/lib/libFindBadConstructs.so'
    myconf_gn+=" is_clang=false"

    # Make sure that -Werror doesn't get added to CFLAGS by the build system.
    # Depending on GCC version the warnings are different and we don't want
    # the build to fail because of that.
    myconf_gn+=" treat_warnings_as_errors=false"

    # Disable fatal linker warnings, Gentoo bug 506268
    myconf_gn+=" fatal_linker_warnings=false"

    # Follow upstream advice on the chromium-packagers group
    myconf_gn+=" fieldtrial_testing_like_official_build=true"

    # FFmpeg handling
    ffmpeg_branding="$(option bindist Chromium Chrome)"
    myconf_gn+=" proprietary_codecs=$(option bindist false true)"
    myconf_gn+=" ffmpeg_branding=\"${ffmpeg_branding}\""

    # Options
    myconf_gn+=" enable_hangout_services_extension=true"
    myconf_gn+=" enable_widevine=true"
    myconf_gn+=" link_pulseaudio=$(option pulseaudio true false)"
    myconf_gn+=" use_cups=$(option cups true false)"
    myconf_gn+=" use_gnome_keyring=$(option keyring true false)"
    myconf_gn+=" use_pulseaudio=$(option pulseaudio true false)"

    # hard disable kerberos support (requires heimdal)
    myconf_gn+=" use_kerberos=false"

    # GTK3 build
    myconf_gn+=" use_gtk3=true"

    # See third_party/BUILD.gn, required for chromium to search for system harfbuzz
    myconf_gn+=" use_system_harfbuzz=true"

    # Jumbo/Unity builds: https://chromium.googlesource.com/chromium/src/+/master/docs/jumbo.md
    # Compile considerably faster (~50% faster ) at the expense of using more RAM,
    # if you run into OOM you can try to set jumbo_file_merge_limit to a lower limit.
    # Fails to build as of 65.0.3325.146
    myconf_gn+=" use_jumbo_build = false"

    # Set up Google API keys, see https://www.chromium.org/developers/how-tos/api-keys
    # Note: These are for Exherbo use ONLY. For your own distribution, please get
    # your own set of keys.
    local google_api_key="AIzaSyBEuNWzExJ_qIX-NT8OY_RGN6d6ZLWnExA"
    local google_default_client_id="750749113475.apps.googleusercontent.com"
    local google_default_client_secret="hw21UnHlcmzSMxzS6VACbPTq"
    myconf_gn+=" google_api_key=\"${google_api_key}\""
    myconf_gn+=" google_default_client_id=\"${google_default_client_id}\""
    myconf_gn+=" google_default_client_secret=\"${google_default_client_secret}\""

    # GN configure
    edo tools/gn/bootstrap/bootstrap.py -v --no-clean --gn-gen-args "${myconf_gn}"
    edo out/Release/gn gen --args="${myconf_gn}" out/Release
}

chromium_src_compile() {
    if optionq suid-sandbox; then
        eninja -C out/Release mksnapshot chrome chrome_sandbox
    else
        eninja -C out/Release mksnapshot chrome
    fi
}

chromium_src_test() {
    local disabled_base_tests=(
        FilePathTest.FromUTF8Unsafe_And_AsUTF8Unsafe            # Needs a utf8 locale
        LoggingTest.CheckStreamsAreLazy                         # does not like to be sandboxed
        OutOfMemoryDeathTest.ViaSharedLibraries
        ProcessMetricsTest.GetNumberOfThreads
        RTLTest.WrapPathWithLTRFormatting
        StatsTableTest.MultipleThreads                          # Flaky
        # Need access to a running X11 server (and potentially more)
        'MessageLoopTestTypeUI.*'
        MessageLoopTest.IsType
        'WaitableEventWatcherTest.*'
        'MessagePumpGLibTest.*'
        'MessagePumpLibeventTest.*'
        'TimerTest.*'
        # fails with "out of memory" even with plenty free mem
        SecurityTest.MemoryAllocationRestrictionsNew
        SecurityTest.MemoryAllocationRestrictionsNewArray
        SecurityTest.NewOverflow
        # Fails randomly on different systems
        SysInfoTest.MaxSharedMemorySize
        TraceEventTestFixture.TraceSampling
        # 42.0.2311.135
        ProcessUtilTest.CurrentDirectory
    )
    local disabled_crypto_tests=(
        # 39.0.2171.95
        SignatureVerifierTest.VerifyRSAPSS
    )
    local disabled_gn_tests=(
        BuilderTest.BasicDeps
        # 41.0.2272.76
        Label.Resolve
    )
    local disabled_ui_base_tests=(
        # Needs fonts/corefonts
        FontTest.LoadArial

        FontTest.GetActualFontNameForTesting

        # Need access to a running X11 server (and potentially more)
        'CanvasTest.*'
        'ClipboardTest.*'
        'ClipboardTest/0.*'
        'FontTest.AvgCharWidth'
        'FontTest.AvgWidths'
        'FontTest.Widths'
        'GtkExpandedContainerTest.*'
        'RenderTextTest.*'
        'ResourceBundleTest.DelegateGetFontList'
        'ScreenTest.*'
        'TextEliderTest.*'
        'TextUtilsTest.GetStringWidth'
        'AnimationContainerTest.*'
        'AnimationTest.*'
        'SlideAnimationTest.*'
        'OSExchangeDataProviderAuraX11Test.*'
        'InputMethodBaseTest.*'
        'OSExchangeDataTest.*'

        # These have been failing for a long time but are not critical. http://crbug.com/110711
        FontListTest.FontDescString_FromFontVector
        FontListTest.Fonts_DescStringWithStyleInFlexibleFormat_RoundTrip
        FontListTest.Fonts_FontVector_RoundTrip
        FontListTest.Fonts_FromDescString
        FontListTest.Fonts_FromDescStringInFlexibleFormat
        FontListTest.Fonts_FromDescStringWithStyleInFlexibleFormat
        FontListTest.Fonts_FromFontVector
        FontListTest.Fonts_GetHeight_GetBaseline
        FontListTest.Fonts_Derive
        FontListTest.Fonts_DeriveWithSizeDelta

        # 38.0.2125.101
        SelectionRequestorTest.NestedRequests
    )

    local disabled_media_tests=(
        # http://crbug.com/160384
        'YUVConvertTest.*'
    )

    local t suites=( base cacheinvalidation crypto gn gpu printing ui_base url ) HOME="${TEMP}"

    for t in ${suites[@]}; do
        tv=disabled_${t}_tests[*]
        eninja -C out/Release ${t}_unittests
        edo out/Release/${t}_unittests --gtest_filter=$(IFS=:; echo "-${!tv}")
    done
}

chromium_src_install() {
    # Install into /opt
    local dest=/opt/chromium
    edo cd out/Release

    exeinto ${dest}
    doexe chrome

    if optionq suid-sandbox; then
        newexe chrome_sandbox chrome-sandbox # the name is hardcoded
        edo chmod 4755 "${IMAGE}"${dest}/chrome-sandbox
    fi
    doexe "${FILES}"/chromium-launcher.sh

    insinto ${dest}
    doins -r *.pak locales resources
    doins -r *.bin

    doins libwidevinecdmadapter.so

    dodir /usr/$(exhost --target)/bin
    dosym ${dest}/chromium-launcher.sh /usr/$(exhost --target)/bin/chromium-browser

    insinto ${dest}/swiftshader
    doins swiftshader/*.so

    for size in 22 24 48 64 128 256 ; do
        insinto /usr/share/icons/hicolor/${size}x${size}/apps
        newins "${WORK}"/chrome/app/theme/chromium/product_logo_${size}.png chromium-browser.png
    done

    insinto /usr/share/applications/
    hereins chromium-browser.desktop <<EOF
[Desktop Entry]
Version=1.0
Name=Chromium
GenericName=Web Browser
Exec=chromium-browser %U
Terminal=false
Icon=chromium-browser
Type=Application
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml_xml;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp;x-scheme-handler/mailto;x-scheme-handler/webcal;
StartupWMClass=Chromium-browser
EOF

    insinto /etc/revdep-rebuild
    hereins ${PN} <<EOF
SEARCH_DIRS=${dest}
EOF
}

chromium_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

chromium_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

